<!-- TOC -->

- [关于](#关于)
  - [项目用途](#项目用途)
  - [说明文档](#说明文档)
    - [参数非空](#参数非空)
    - [额外参数](#额外参数)
  - [结构化对象](#结构化对象)
    - [名称组成部分说明](#名称组成部分说明)
    - [ObjectList](#objectlist)
    - [ItemList](#itemlist)
    - [ArrayItemList](#arrayitemlist)
  - [下一步](#下一步)
- [版本说明](#版本说明)
  - [2021](#2021)
    - [9.30](#930)
    - [8.10](#810)
    - [8.5](#85)
    - [7.19](#719)
    - [7.6](#76)
    - [7.5](#75)
    - [7.3](#73)
    - [6.30](#630)
    - [6.29](#629)

<!-- /TOC -->
# 关于

## 项目用途

> 该项目用于对JS Object对象进行渲染/转换成HTML Document对象.

## 说明文档

详见[示例页面](https://f97d1c.gitlab.io/graphic-object/dist/index.html).

### 参数非空

示例页面参数说明中,默认值为无的表示参数必传,其他参数为可选.

### 额外参数

如果传递文档中未提及的参数,那么该参数将用于渲染对象的创建过程,作为toElement的参数.

## 结构化对象

> 将具有某一固定结构的Object或者Array进行细分命名,并将其作为单一整体进行描述及处理.<br>
后期可以达到根据提供的数据对象自动适配对应可视化图表功能.

### 名称组成部分说明

名称部分|含义
-|-
List|集合形式
Object|1.元素类型为对象<br>2.各对象构成属性统一
Objects|1.元素类型为对象<br>2.各对象构成属性*不*统一
Array|1.元素类型为对象<br>2.元素类型保持一致
Arrays|1.元素类型为对象<br>2.元素类型*不*一致
Item|元素类型一致且不能为数组(Array)或对象(Object)类型

### ObjectList

> 由统一结构对象(object)组成的集合.

```js
[
  {key: 'value1'},
  {key: 'value2'}
]
```

### ItemList

> 由统一数据类型元素组成的集合.

```js
['value1','value2']
```

### ArrayItemList

示例|是否正确
-|-
[['下单时间','日期'],['商城订单号','关键词']]|√
[['下单时间','日期'],['商城订单号',2]]|X
[[1,3],[5,2]]|√
[[1,3],5,2]|X

## 下一步

0. 新增 -功能-> [Bootstrap5#modal](https://getbootstrap.com/docs/5.0/components/modal)
0. 导出表格功能根据ObjectList进行优化
0. 说明文件优化添加调用示例.
0. 接口文档兼容EXCEL模块类型接口.
0. 接口文档集成至说明文档.
0. amchart4外部引用异常问题原因排查.

# 版本说明

## 2021

### 9.30

模块|方法|类型|调整内容
-|-|-|-
Base|base.css|删除|全局body样式去除.
package.json|-|增加|nodemon-webpack-plugin
ObjectTypes|_typeof|调整|dateTime类型判断条件优化
Bootstrap5|baseTable|调整|参数: 表单样式类 -更名-> 表格样式类
Bootstrap5|baseTable|优化|如果渲染对象中属性键为undefined/null/nil, 将不显示td标签,td值将跨两列展示.
Bootstrap5|baseTable|优化|属性值中type为hidden,整行将隐藏不显示(tr.style.display = 'none')
Bootstrap5|listTable|优化|参数 -新增-> 数组格式化模式
Bootstrap5|formTable|调整|原formTable更名为esSearchTable
Bootstrap5|formTable|新增|新formTable用于表单提交
### 8.10

模块|方法|类型|调整内容
-|-|-|-
Bootstrap5|baseTable|新增参数|同行限制个数 -作用-> td内包含的元素个数小于该值时将元素在一行内显示,大于时则进行自动换行
Base|ItemList|新增|
Base|ArrayItemList|新增|

### 8.5

模块|方法|类型|调整内容
-|-|-|-
Base|md5|新增|返回给定字符串的MD5值
Base|ObjectList|新增|将数组转换成ObjectList对象
Bootstrap5|listTable|优化|参数: 最大宽度, 非自动换行模式时,当元素长度大于该值时隐藏省略过长内容<br>并添加title参数显示全文已经onclick点击alert显示.

### 7.19

模块|方法|调整内容
-|-|-
Base|sudoku|全屏方法根据浏览器类型选择requestFullscreen或webkitRequestFullscreen<br>Safair目前退出全屏后会存在元素未按比例缩小展示问题<br>目前F12调试会页面卡死,浏览器异常退出,暂时遗留
Browser|browserName|新增 根据浏览器制造商推断浏览器名称
Bootstrap5|listTable|横向滚动表格高度根据元素个数判断<br>height = (described.渲染对象.length + 1) * 7<br>height = (height > 80) ? 80 : height

### 7.6

外部引入名称调整:

```js
module.exports = {
  // ...
  output: {
    // ...
    library: {
-     name: "GraphicObject",
+     name: "graphic-object",
      type: 'umd',
    },
  }
  // ...
}
```

### 7.5

模块|方法|调整内容
-|-|-
Excel|toXlsx|页签特殊字符过滤(* ? : \ / [ ])
Bootstrap5|listTable|表单样式类参数: 默认值由 mt-3 -> mt-5
Bootstrap5|listTable|设置横向滚动条件改为:集合内元素大于30条或首条元素字节长度大于160
Bootstrap5|listTable|横向滚动添加表格高度参数: height: 70vh
Bootstrap5|range_input|添加title参数,取值同placeholder.
Excel|toXlsx|默认活动页签改为第一个: workbook.views[0].activeTab = 0

### 7.3

主要涉及Bootstrap4升级至Bootstrap5导致不兼容样式调整.

模块|调整内容
-|-
Bootstrap4|更名为 Bootstrap5
Bootstrap5|mr-* 样式名变为 me-*
Bootstrap5|form-inline 样式名变为 row和col
Bootstrap5|表格水平滚动样式table-responsive改为外层Div
Bootstrap5|表格添加默认样式类align-middle,起到所有行元素默认居中效果
Excel|添加对象转为xlsx格式表格方法:toXlsx
-|说明文档添加.

### 6.30

0. amchart4外部调用异常问题(暂时剔除).

### 6.29

0. 原有Graphic项目衍生出Webpack框架下GraphicObject项目.
0. NPM包发布