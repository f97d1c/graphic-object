const path = require('path');
const webpack = require('webpack');
const NodemonPlugin = require('nodemon-webpack-plugin');

module.exports = {
  // mode: 'development',
  mode: 'production',
  entry: './src/init.mjs',
  output: {
    publicPath: '/',
    filename: 'graphic-object-core.js',
    path: path.resolve(__dirname, 'dist'),
    library: {
      // 外部引入名称: import Graphic from 'graphic-object'
      name: "graphic-object",
      type: 'umd',
    },
  },
  module: {
    rules: [
      {
        test: /\.(png|jpe?g|gif|svg|eot|ttf|woff|woff2)$/i,
        // More information here https://webpack.js.org/guides/asset-modules/
        type: "asset/inline",
      },
      {
        test: /\.css$/, use: ['style-loader', 'css-loader']
      },      
      {
        test: /jquery.+\.js$/,
        use: [
          {
            loader: 'expose-loader',
            options: {
              exposes: ["$", "jQuery"]
            }
          }
        ]
      },
      {
        test: /\.js$/,
        enforce: "pre",
        use: ["source-map-loader"],
      },
      {
        test: /\.s[ac]ss$/i,
        use: ["style-loader", "css-loader", "sass-loader"],
      },
    ]
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
      Popper: ['popper.js', 'default'],
    }),
    new NodemonPlugin(), // Dong
  ],
  externals: {
    lodash: {
      commonjs: 'lodash',
      commonjs2: 'lodash',
      amd: 'lodash',
      root: '_',
    },
  },
};
