import Base from './base.mjs'
// ['日期','关键词']   √
// ['日期','关键词',1] X
export default class ItemList extends Base {

  constructor(array = []) {
    super();
    this.originalObject = array
  }

  // 针对数组各元素提供统一的数据类型及其他信息
  info(originalObject = []){
    this.maxLength_(this.originalObject)
    return this
  }

  maxLength_(originalObject = []) {
    if (!!this.maxLength) return this.maxLength;

    let columnLength = originalObject.map(item => {return (item||'').toString().length})
    this.maxLength = Math.max(...columnLength)

    return this.maxLength
  }
}

