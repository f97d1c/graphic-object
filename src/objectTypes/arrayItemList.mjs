import Base from './base.mjs'
export default class ArrayItemList extends Base {
  constructor(array = []) {
    super();
    this.originalObject = array
  }

  // 将元素拼接为下标元素对齐的字符串
  polishStr(params) {
    if (!!this.polishStr_) return [true, this.polishStr_];

    let defaultValue = {
      补位符号: '　', //全角空格
      间隔长度: 2,
      字符类型: '全角',
    }
    params = Object.assign(defaultValue, params)

    let itemLength = this.originalObject.map(item => { return (item || []).length })
    let maxLength = Math.max(...itemLength)
    let columnMaxLength = []

    let formatedObject = []

    this.originalObject.forEach(array => {
      let tmp = []
      array.forEach(item=>{
      tmp.push(this.formatStrCode({字符类型: params.字符类型, 字符串: item}))
      })
      formatedObject.push(tmp)
    })

    // 最后一个元素不管
    for (let i = 0; i < (maxLength - 1); i++) {
      itemLength = formatedObject.map(item => { return (item || [])[i].toString().length })
      columnMaxLength[i] = Math.max(...itemLength)
    }

    this.polishStr_ = []
    formatedObject.forEach(item => {
      let tmp = ''
      for (let i = 0; i < item.length; i++) {
        if (!!columnMaxLength[i]) {
          tmp += item[i].toString().padEnd((columnMaxLength[i] + params.间隔长度), params.补位符号)
        } else {
          tmp += item[i].toString()
        }
      }
      this.polishStr_.push(tmp)
    })

    return [true, this.polishStr_]
  }

  formatStrCode (params) {
    let defaultValue = {
      字符类型: '全角',
      字符串: undefined
    }
    params = Object.assign(defaultValue, params)

    let formatStr= ''
    let content = params.字符串
    for (let i = 0; i < content.length; i++) {

      if (content.charCodeAt(i) >= 0 && content.charCodeAt(i) <= 255) {
        if (params.字符类型 == '半角'){
          formatStr+=content[i]
        }else{
          formatStr+=this.toSBC(content[i])
        }
      }
      else {
        if (params.字符类型 == '全角'){
          formatStr+=content[i]
        }else{
          formatStr+=this.toDBC(content[i])
        }
      }
    }
    return formatStr
  }
  
  // 半角转化为全角
  toSBC(string){
    let result = "";
    for(let i=0; i<string.length; i++){
      if(string.charCodeAt(i) == 32){
        result += String.fromCharCode(12288);
      }else if(string.charCodeAt(i) > 32 && string.charCodeAt(i) < 127){
        result += String.fromCharCode(string.charCodeAt(i) + 65248);
      }else{
        result += String.fromCharCode(string.charCodeAt(i));
      }
    }
    return result;
  }
  
  // 全角转化为半角
  toDBC(string){
    let result = "";
    for(let i=0; i<string.length; i++){
      if(string.charCodeAt(i) == 12288){
        result += String.fromCharCode(32);
      }else if(string.charCodeAt(i) > 65280 && string.charCodeAt(i) < 65375){
        result += String.fromCharCode(string.charCodeAt(i) - 65248);
      }else{
        result += String.fromCharCode(string.charCodeAt(i));
      }
    }
    return result;
  }

}

