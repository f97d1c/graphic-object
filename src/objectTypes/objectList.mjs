import Base from './base.mjs'

export default class ObjectList extends Base {

  constructor(array = []) {
    super();
    this.originalObject = array
  }

  // 针对数组各元素提供统一的数据类型及其他信息
  info(originalObject = []){
    this.typeof_(this.originalObject)
    this.maxLength_(this.originalObject)
    return this
  }

  // 根据各元素同一属性值不同的类型 给出统一的数据类型
  typeof_(originalObject = []) {
    if (!!this.typeof) return this.typeof;

    let typeof_ = this.typeof = {}
    for (let [key, value] of Object.entries(originalObject[0])) {
      typeof_[key] = this._typeof(value)
    }

    let getColumnTypes = (key) => {
      let tmp = []
      originalObject.forEach(item => {
        tmp.push(this._typeof(item[key]))
      })
      return [...new Set(tmp)]
    }

    let columnTypesIn = (column, types) => {
      let self = this
      return originalObject.every(function (object) {
        return types.includes(self._typeof(object[column]))
      })
    }

    for (let [key, value] of Object.entries(typeof_)) {
      switch (value) {
        case 'integer':
        case 'decimal(2)':
        case 'decimal(2+)':
          let sortTypes = ['decimal(2+)', 'decimal(2)', 'integer']
          let boolean = columnTypesIn(key, sortTypes)
          if (!boolean) {
            typeof_[key] = 'string'
            break
          }
          let types = getColumnTypes(key)

          for (let index in sortTypes) {
            let type = sortTypes[index]
            if (types.includes(type)) {
              typeof_[key] = type
              break
            }
          }

          break;
        default:
          break;
      }
    }

    return typeof_
  }

  maxLength_(originalObject = []) {
    if (!!this.maxLength) return this.maxLength;

    let maxLength_ = this.maxLength = {}
    for (let [key, value] of Object.entries(originalObject[0])) {
      let columnLength = originalObject.map(item => {return (item[key]||'').toString().length})
      maxLength_[key] = Math.max(...columnLength)
    }

    return maxLength_
  }
}

