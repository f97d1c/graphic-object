import Graphic from '../graphic.mjs';
// import './core.js';
// import './tooltip.css';
// import './util.css';
// import './orgchart.css';
// import './default_module.js';
// import './graphics_module.js';
// import './ui_module.js';
// import './orgchart_module.js';
// import './wordtree_module.js';

export default class GoogleCharts extends Graphic {
  constructor(params={}) {
    super()
  }

  wordTree(params={}) {
    let described = this.described.initialize({
      名称: '词树',
      描述: '根据提供的关键词组织成树结构',
      参数: {
        渲染对象: { 类型: 'array', 描述: '将存在层级结构的词组按空格分隔,以每个最小独立单元组成数组', 默认值: undefined },
        根词组: { 类型: 'string', 描述: '将以此词组为顶点进行组织', 默认值: undefined },
      }
    })
    if (!!params.showHow) return [true, described]

    let res = described.validate(params)
    if (!res[0]) return res

    res = described.createEle(Object.assign({
    }, params))
    if (!res[0]) return res

    let formatedData = []
    described.渲染对象.forEach(item => {
      formatedData.push([item])
    });

    google.charts.load('current', { packages: ['wordtree'] });
    google.charts.setOnLoadCallback(() => {
      let data = google.visualization.arrayToDataTable(formatedData);

      let options = {
        wordtree: {
          format: 'implicit',
          word: described.根词组
        }
      };

      let chart = new google.visualization.WordTree(described.element);
      chart.draw(data, options);
    })

    return [true, described.element]
  }

  orgChart(params={}) {
    let described = this.described.initialize({
      名称: '组织结构图',
      描述: '展示节点层次结构,通常用于描述组织中的上级/下级关系.',
      参数: {
        渲染对象: { 类型: 'array', 描述: '每个数组由三部分组成[{"v":"子节点引用标识", "f":"显示名称"},"父节点标识", "说明"]或["显示名称","父节点标识", "说明"]', 默认值: undefined },
      }
    })
    if (!!params.showHow) return [true, described]

    let res = described.validate(params)
    if (!res[0]) return res

    res = described.createEle(Object.assign({
    }, params))
    if (!res[0]) return res

    let styleElement = document.createElement('style')
    styleElement.appendChild(document.createTextNode(''))
    document.head.appendChild(styleElement)
    let sheet = styleElement.sheet
    sheet.insertRule('.google-visualization-orgchart-node{border: none !important;}', 0)

    google.charts.load('current', { packages: ["orgchart"] });
    google.charts.setOnLoadCallback(() => {
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Name');
      data.addColumn('string', 'Manager');
      data.addColumn('string', 'ToolTip');

      // For each orgchart box, provide the name, manager, and tooltip to show.
      data.addRows(described.渲染对象);

      // Create the chart.
      var chart = new google.visualization.OrgChart(described.element);
      // Draw the chart, setting the allowHtml option to true for the tooltips.
      chart.draw(data, { 'allowHtml': true });
    });

    return [true, described.element]
  }
}