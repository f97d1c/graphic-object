import Graphic from '../graphic.mjs';

// import 'highcharts'

// import highcharts from '../highcharts8/highcharts.js';
import Highcharts from '../highcharts8/highcharts.js';
import TimeLine from '../highcharts8/timeline.js';
TimeLine(Highcharts);
import Exporting from '../highcharts8/exporting.js';
Exporting(Highcharts);
import Accessibility from '../highcharts8/accessibility.js';
Accessibility(Highcharts)
import Data from '../highcharts8/data.js';
Data(Highcharts);
import Boost from '../highcharts8/boost.js';
Boost(Highcharts);
import BoostCanvas from '../highcharts8/boost-canvas.js';
BoostCanvas(Highcharts);
import zh_CN from '../highcharts8/zh_CN.js';
zh_CN(Highcharts);

export default class Highcharts8 extends Graphic {
  constructor(params={}) {
    super()
  }

  columnParsed(params={}) {
    let described = this.described.initialize({
      名称: '数据列分析表格',
      描述: '将HTML表格用作图表的数据源,通过引用页面中现有的HTML数据表来构建图表.',
      参数: {
        计量单位: { 类型: 'string', 描述: 'Y轴数字计量单位名称.', 默认值: '百分比(%)' },
      }
    })
    if (!!params.showHow) return [true, described]

    let res = described.validate(params)
    if (!res[0]) return res

    let containerId = this.uuid()
    let tableId = this.uuid()
    res = described.createEle(Object.assign({
      element: 'figure',
      class: 'highcharts-figure',
      innerHtml: { element: 'div', id: containerId }
    }, params))
    if (!res[0]) return res

    res = window['Graphic'].bootstrap5.compareTable(Object.assign(params, {
      父元素: described.element,
      元素ID: tableId
    }))
    if (!res[0]) return res

    Highcharts.chart(containerId, {
      data: {
        table: tableId
      },
      chart: {
        type: 'column'
      },
      title: {
        text: described.标题
      },
      subtitle: {
        text: described.副标题
      },
      yAxis: {
        allowDecimals: false,
        title: {
          text: described.计量单位
        }
      },
      tooltip: {
        formatter: function () {
          return '<b>' + this.series.name + '</b><br/>' +
            this.point.y + ' ' + this.point.name.toLowerCase();
        }
      }
    })


    return [true, described.element]
  }

  timeLine(params={}) {
    let described = this.described.initialize({
      名称: '时间线图',
      描述: '时间线图表用于在时间轴上放置事件.',
      参数: {
        渲染对象: { 类型: 'array', 描述: '数组中按照时间顺序进行排列', 默认值: undefined },
        组成颜色: { 类型: 'array', 描述: '各时间节点的颜色区分,和渲染对象的键值对顺序保持一致', 默认值: ['#007bff', '#6610f2', '#6f42c1', '#e83e8c', '#dc3545', '#fd7e14', '#ffc107', '#28a745', '#20c997', '#17a2b8', '#fff', '#6c757d', '#343a40', '#007bff', '#6c757d', '#28a745', '#17a2b8', '#ffc107', '#dc3545', '#f8f9fa', '#343a40'] },
      }
    })
    if (!!params.showHow) return [true, described]

    let res = described.validate(params)
    if (!res[0]) return res

    let containerId = this.uuid()
    res = described.createEle(Object.assign({
      element: 'figure',
      class: 'highcharts-figure',
      innerHtml: { element: 'div', id: containerId }
    }, params))
    if (!res[0]) return res

    Highcharts.chart(containerId, {
      chart: {
        type: 'timeline'
      },
      accessibility: {
        screenReaderSection: {
          beforeChartFormat: '<h5>{chartTitle}</h5>' +
            '<div>{typeDescription}</div>' +
            '<div>{chartSubtitle}</div>' +
            '<div>{chartLongdesc}</div>' +
            '<div>{viewTableButton}</div>'
        },
        point: {
          valueDescriptionFormat: '{index}. {point.label}. {point.description}.'
        }
      },
      xAxis: {
        visible: false
      },
      yAxis: {
        visible: false
      },
      title: {
        text: described.标题
      },
      subtitle: {
        text: described.副标题
      },
      // colors: [ '#4185F3', '#427CDD', '#406AB2', '#3E5A8E', '#3B4A68', '#363C46'],
      colors: described.组成颜色,
      series: [{
        data: described.渲染对象
      }]
    })
    return [true, described.element]
  }


}