import Graphic from '../graphic.mjs';
import './core.js';

export default class ChartJs2 extends Graphic {
  constructor(params={}) {
    super()
  }

  doughnut(params={}) {
    let described = this.described.initialize({
      名称: '甜甜圈',
      描述: '根据提供的渲染对象转换成词云',
      参数: {
        图表类型: { 类型: 'string', 描述: '默认甜甜圈(doughnut)形状,可选择饼图(pie)', 默认值: 'doughnut' },
        组成颜色: { 类型: 'array', 描述: '为每部分赋予不同的颜色区分,和渲染对象的键值对顺序保持一致,自动排序状态下会打乱顺序', 默认值: ['#007bff', '#6610f2', '#6f42c1', '#e83e8c', '#dc3545', '#fd7e14', '#ffc107', '#28a745', '#20c997', '#17a2b8', '#fff', '#6c757d', '#343a40', '#007bff', '#6c757d', '#28a745', '#17a2b8', '#ffc107', '#dc3545', '#f8f9fa', '#343a40'] },
        自动排序: { 类型: 'boolean', 描述: '根据渲染对象中参数值进行排序后渲染', 默认值: true },
      }
    })
    if (!!params.showHow) return [true, described]

    let res = described.validate(params)
    if (!res[0]) return res

    if (described.自动排序) {
      let keysSorted = Object.keys(described.渲染对象).sort(function (x, y) { return described.渲染对象[y] - described.渲染对象[x] })
      let tmp = {}
      keysSorted.forEach(item => {
        tmp[item] = described.渲染对象[item]
      })
      described.渲染对象 = tmp
    }

    res = described.createEle(Object.assign({
      element: 'canvas',
    }, params))
    if (!res[0]) return res

    new Chart(described.element.getContext('2d'), {
      type: described.图表类型,
      data: {
        "labels": Object.keys(described.渲染对象),
        "datasets": [
          {
            "data": Object.values(described.渲染对象),
            "backgroundColor": described.组成颜色
          }
        ]
      },
      options: {
        title: {
          display: (!!described.标题),
          text: described.标题
        },
        responsive: true
      }
    })

    return [true, described.element]
  }

  broken_line(params={}) {
    let described = this.described.initialize({
      名称: '折线图',
      描述: '根据提供的Json数据返回对应的折线图图表',
      参数: {
        组成颜色: { 类型: 'array', 描述: '折线间的颜色区分,和渲染对象的键值对顺序保持一致', 默认值: ['#007bff', '#6610f2', '#6f42c1', '#e83e8c', '#dc3545', '#fd7e14', '#ffc107', '#28a745', '#20c997', '#17a2b8', '#fff', '#6c757d', '#343a40', '#007bff', '#6c757d', '#28a745', '#17a2b8', '#ffc107', '#dc3545', '#f8f9fa', '#343a40'] },
        背景色: { 类型: 'string', 默认值: '#0f0e0e1c' },
      }
    })
    if (!!params.showHow) return [true, described]

    let res = described.validate(params)
    if (!res[0]) return res

    res = described.createEle(Object.assign({
      element: 'canvas',
      style: 'background-color: ' + described.背景色,
    }, params))
    if (!res[0]) return res

    let lineDatas = []
    // TODO: 后面加排序 添加各项是否存在缺失key
    let xSorts = Object.keys(Object.values(described.渲染对象)[0])

    for (let [index, [key, value]] of Object.entries(Object.entries(described.渲染对象))) {
      let color = described.组成颜色[index % described.组成颜色.length]

      lineDatas.push({
        label: key,
        data: Object.values(value),
        backgroundColor: color,
        borderColor: color,
        fill: false
      })
    }

    new Chart(described.element.getContext('2d'), {
      type: 'line',
      data: {
        labels: xSorts,
        datasets: lineDatas,
      },
      options: {
        title: {
          display: (!!described.标题),
          text: described.标题
        }
      }
    })

    return [true, described.element]
  }

}