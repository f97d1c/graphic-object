import Graphic from '../graphic.mjs'
// mjs后缀无法导入
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import * as am4plugins_wordCloud from "@amcharts/amcharts4/plugins/wordCloud"; 
import am4lang_zh_Hans from "@amcharts/amcharts4/lang/zh_Hans";

import am4themes_animated from "@amcharts/amcharts4/themes/animated";
am4core.useTheme(am4themes_animated);


export default class Amcharts4 extends Graphic {
  constructor(params={}) {
    super();
  }

  tagCloud(params={}) {
    let described = this.described.initialize({
      名称: '标签云',
      描述: '根据提供的渲染对象转换成词云',
      参数: {
        跳转方式: { 描述: '点击链接跳转方式', 类型: 'string', 默认值: '_blank' },
        跳转链接: { 类型: 'url', 描述: '点击标签后的跳转链接, 链接参数(tag_datas中key_word的值)用{key_word}代替,例如: http://fund.eastmoney.com/{key_word}.html', 默认值: undefined },
      }
    })
    if (!!params.showHow) return [true, described]

    let res = described.validate(params)
    if (!res[0]) return res

    let formatedData = []
    for (let [key, value] of Object.entries(described.渲染对象)) {
      formatedData.push({ "tag": key, "key_word": value.key_word, "weight": value.weight })
    }

    res = described.createEle(Object.assign({
      style: "width: '100%'; height: 300px;",
    }, params))
    if (!res[0]) return res

    am4core.ready(function () {
      am4core.useTheme(am4themes_animated);

      var chart = am4core.create(described.元素ID, am4plugins_wordCloud.WordCloud);
      chart.fontFamily = "Courier New";
      chart.numberFormatter.numberFormat = "#";
      var series = chart.series.push(new am4plugins_wordCloud.WordCloudSeries());
      series.randomness = 0.1;
      series.rotationThreshold = 0.6;

      // 随机分配不同颜色
      series.colors = new am4core.ColorSet();
      series.colors.passOptions = {};

      series.data = formatedData;

      series.dataFields.word = "tag";
      series.dataFields.value = "weight";

      series.heatRules.push({
        "target": series.labels.template,
        "property": "fill",
        // 根据大小分配两种固定颜色
        // "min": am4core.color("#fd7e14"),
        // "max": am4core.color("#dc3545"),
        "dataField": "value"
      });

      series.labels.template.url = described.跳转链接;
      series.labels.template.urlTarget = described.跳转方式;
      // series.labels.template.tooltipText = "{word}: {value}";

      var hoverState = series.labels.template.states.create("hover");
      hoverState.properties.fill = am4core.color("#fd7e");

      // var subtitle = chart.titles.create();
      // subtitle.text = "(click to open)";

      // var title = chart.titles.create();
      // title.text = "Most Popular Tags @ StackOverflow";
      // title.fontSize = 20;
      // title.fontWeight = "800";

    })

    return [true, described.element]
  }

  verticalGraph(params={}) {
    let described = this.described.initialize({
      名称: '垂直条形图',
      描述: '主要用于横轴内容较多时采用纵向排列以确保页面安全',
      参数: {}
    })
    if (!!params.showHow) return [true, described]

    let res = described.validate(params)
    if (!res[0]) return res

    let formatedData = []
    for (let [key, value] of Object.entries(described.渲染对象)) {
      formatedData.push({name: key, value: value})
    }

    res = described.createEle(Object.assign({
      style: ("width: '100%'; height: " + formatedData.length * 70 + 'px;')
    }, params))
    if (!res[0]) return res

    am4core.ready(function () {
      // Themes begin
      am4core.useTheme(am4themes_animated);
      // Themes end

      var chart = am4core.create(described.element.id, am4charts.XYChart);
      chart.padding(40, 40, 40, 40);

      let title = chart.titles.create();
      title.text = described.标题
      title.fontSize = 16
      title.marginBottom = 18

      var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
      categoryAxis.renderer.grid.template.location = 0;
      categoryAxis.dataFields.category = "name";
      categoryAxis.renderer.minGridDistance = 1;
      categoryAxis.renderer.inversed = true;
      categoryAxis.renderer.grid.template.disabled = true;

      var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
      valueAxis.min = 0;

      var series = chart.series.push(new am4charts.ColumnSeries());
      series.dataFields.categoryY = "name";
      series.dataFields.valueX = "value";
      series.tooltipText = "{valueX.value}"
      series.columns.template.strokeOpacity = 0;
      series.columns.template.column.cornerRadiusBottomRight = 5;
      series.columns.template.column.cornerRadiusTopRight = 5;

      var labelBullet = series.bullets.push(new am4charts.LabelBullet())
      labelBullet.label.horizontalCenter = "left";

      labelBullet.label.dx = 10;
      // labelBullet.label.text = "{values.valueX.workingValue.formatNumber('#.0as')}";
      labelBullet.label.text = "{values.valueX.workingValue.formatNumber('#.00')}";
      labelBullet.locationX = 1;

      // as by 默认值 columns of the same series are of the same color, we add adapter which takes colors from chart.colors color set
      series.columns.template.adapter.add("fill", function (fill, target) {
        return chart.colors.getIndex(target.dataItem.index);
      });

      categoryAxis.sortBySeries = series;
      chart.data = formatedData

    })

    return [true, described.element]
  }

  pyramid(params={}) {
    let described = this.described.initialize({
      名称: '金字塔',
      描述: '将数据占比转换为金字塔表示',
      参数: {}
    })
    if (!!params.showHow) return [true, described]

    let res = described.validate(params)
    if (!res[0]) return res

    let formatedData = []
    for (let [key, value] of Object.entries(described.渲染对象)) {
      formatedData.push({ "name": key, "value": value })
    }

    res = described.createEle(params)
    if (!res[0]) return res

    let style = described.element.getAttribute('style') || "width: '100%'; height: " + formatedData.length * 70 + 'px;'
    described.element.setAttribute('style', style)

    document.body.style['font-family'] = '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"'

    am4core.ready(function () {
      // Themes begin
      am4core.useTheme(am4themes_animated);
      // Themes end

      var chart = am4core.create(described.element.id, am4charts.SlicedChart);
      chart.paddingBottom = 30;
      chart.data = formatedData;

      var series = chart.series.push(new am4charts.PyramidSeries());
      series.dataFields.value = "value";
      series.dataFields.category = "name";
      series.alignLabels = true;
      series.valueIs = "height";

    })

    return [true, described.element]
  }

  candlestick(params={}) {
    let described = this.described.initialize({
      名称: '蜡烛图',
      描述: '用于生成包含开盘价,最高价,最低价以及收盘价在内的蜡烛图.',
      参数: {
        渲染对象: { 类型: 'array', 默认值: undefined },
        趋势颜色: { 类型: 'array', 描述: '["开盘价高于收盘价颜色", "开盘价低于收盘价颜色"]', 默认值: ["red", "green"] },
        提示信息: { 类型: 'string', 描述: '鼠标放在节点上时显示信息', 默认值: "开盘:{openValueY.value}\n最低:{lowValueY.value}\n最高:{highValueY.value}\n收盘:{valueY.value}" },
        时间格式: { 类型: 'string', 默认值: 'yyyy-MM-dd' },
        背景色: { 类型: 'string', 默认值: '#0f0e0e1c' },
      }
    })

    if (!!params.showHow) return [true, described]

    let res = described.validate(params)
    if (!res[0]) return res

    res = described.createEle(Object.assign({
      style: "width: 100%; height: 500px; background-color: " + described.背景色,
    }, params))
    if (!res[0]) return res

    am4core.ready(function () {

      // Themes begin
      am4core.useTheme(am4themes_animated);
      // Themes end

      var chart = am4core.create(described.element.id, am4charts.XYChart);
      chart.paddingRight = 20;
      chart.language.locale = am4lang_zh_Hans;

      chart.dateFormatter.inputDateFormat = described.时间格式;

      var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
      dateAxis.renderer.grid.template.location = 0;
      dateAxis.dateFormats.setKey("day", "MMMM dt");
      dateAxis.periodChangeDateFormats.setKey("day", "MMMM dt");

      var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
      valueAxis.tooltip.disabled = true;

      var series = chart.series.push(new am4charts.CandlestickSeries());
      series.dataFields.dateX = "日期";
      series.dataFields.valueY = "收盘价";
      series.dataFields.openValueY = "开盘价";
      series.dataFields.lowValueY = "盘中最低";
      series.dataFields.highValueY = "盘中最高";
      series.simplifiedProcessing = true;
      series.tooltipText = described.提示信息;

      series.riseFromOpenState.properties.fill = described.趋势颜色[0];
      series.riseFromOpenState.properties.stroke = described.趋势颜色[0];
      series.dropFromOpenState.properties.fill = described.趋势颜色[1];
      series.dropFromOpenState.properties.stroke = described.趋势颜色[1];

      chart.cursor = new am4charts.XYCursor();

      // a separate series for scrollbar
      var lineSeries = chart.series.push(new am4charts.LineSeries());
      lineSeries.dataFields.dateX = "日期";
      lineSeries.dataFields.valueY = "收盘价";
      // need to set on default state, as initially series is "show"
      lineSeries.defaultState.properties.visible = false;

      // hide from legend too (in case there is one)
      lineSeries.hiddenInLegend = true;
      lineSeries.fillOpacity = 0.5;
      lineSeries.strokeOpacity = 0.5;

      var scrollbarX = new am4charts.XYChartScrollbar();
      scrollbarX.series.push(lineSeries);
      chart.scrollbarX = scrollbarX;

      chart.data = described.渲染对象;

    })

    return [true, described.element]
  }


  broken_timeline(params={}) {
    let described = this.described.initialize({
      名称: '折线图(可伸缩时间轴)',
      描述: '根据提供的渲染对象返回可伸缩时间轴的折线图表',
      参数: {
        组成颜色: { 类型: 'array', 描述: '折线间的颜色区分,和渲染对象的键值对顺序保持一致', 默认值: ["#007bff", "#6610f2", "#6f42c1", "#e83e8c", "#dc3545", "#fd7e14", "#ffc107", "#28a745", "#20c997", "#17a2b8"] },
        背景色: { 类型: 'string', 默认值: '#0f0e0e1c' },
        时间格式: { 类型: 'string', 默认值: 'yyyy-MM-dd' },
        提示信息: { 类型: 'string', 描述: '鼠标放在节点上时显示信息', 默认值: "{date}:{value}" },
      }
    })
    if (!!params.showHow) return [true, described]

    let res = described.validate(params)
    if (!res[0]) return res

    res = described.createEle(Object.assign({
      style: 'height: 500px; background-color: ' + described.背景色,
    }, params))
    if (!res[0]) return res

    // datas << {date:date, value: value}
    let formatedData = []
    for (let [key, value] of Object.entries(described.渲染对象)) {
      formatedData.push({ date: key, value: value })
    }

    am4core.ready(function () {

      // Themes begin
      am4core.useTheme(am4themes_animated);
      // Themes end

      var chart = am4core.create(described.element.id, am4charts.XYChart);
      chart.dateFormatter.inputDateFormat = described.时间格式;

      chart.data = formatedData;
      chart.language.locale = am4lang_zh_Hans;

      var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
      dateAxis.renderer.grid.template.location = 0;
      dateAxis.dateFormats.setKey("day", "MMMM dt");
      dateAxis.periodChangeDateFormats.setKey("day", "MMMM dt");

      var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

      // Create series
      var series = chart.series.push(new am4charts.LineSeries());
      series.dataFields.valueY = "value";
      series.dataFields.dateX = "date";
      series.tooltipText = described.提示信息

      series.tooltip.pointerOrientation = "vertical";

      chart.cursor = new am4charts.XYCursor();
      chart.cursor.snapToSeries = series;
      chart.cursor.xAxis = dateAxis;

      //chart.scrollbarY = new am4core.Scrollbar();
      chart.scrollbarX = new am4core.Scrollbar();

    })

    return [true, described.element]
  }

}