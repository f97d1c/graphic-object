
import Graphic from './graphic.mjs';
window.Graphic = new Graphic();

import Bootstrap5 from './bootstrap5/bootstrap5.mjs'
window.Graphic.bootstrap5 = new Bootstrap5();

import ChartJs2 from './chartJs2/chartJs2.mjs'
window.Graphic.chartJs2 = new ChartJs2();

// 在插件内部可以使用 外部插件安装后报错: 可能与开源政策有关 暂时去掉
// Uncaught TypeError: Cannot read property '1' of null
// import Amcharts4 from './amcharts4/amcharts4.js'
// window.Graphic.amcharts4 = new Amcharts4();

import GoogleCharts from './googleCharts/googleCharts.mjs'
window.Graphic.googleCharts = new GoogleCharts();

import Highcharts8 from './highcharts8/highcharts8.mjs'
window.Graphic.highcharts8 = new Highcharts8();

import Excel from './excel/excel.mjs'
window.Graphic.excel = new Excel();
