export default class Browser {
  // 根据制造商判断浏览器名称
  browserName(){
    let browserName = ''
    let vender = window.navigator.vendor
    switch (vender) {
      case 'Google Inc.':
        browserName = 'chrome'
        break;
      case 'Apple Computer, Inc.':
        browserName = 'safair'
        break;
      default:
        console.error('尚未定义制造商:'+vender+'对应浏览器名称')
        browserName = '*'
        break;
    }
    return browserName
  }
}