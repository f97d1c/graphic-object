import Described from './described.mjs';
import Browser from './browser.mjs';
import Md5 from 'md5';
import ObjectTypesBase from '../objectTypes/base.mjs'
import ObjectList from '../objectTypes/objectList.mjs'
import ItemList from '../objectTypes/itemList.mjs'
import ArrayItemList from '../objectTypes/arrayItemList.mjs'

export default class Base {

  constructor(params = {}) {
    this.described = Described;
    this.browser = new Browser();
    this.md5 = Md5;
    this.ObjectList = ObjectList;
    this.ItemList = ItemList;
    this.ArrayItemList = ArrayItemList;
    
    ['_typeof', 'multiType'].forEach(method => {
      this[method] = (params) => { return (new ObjectTypesBase())[method](params) }
    })
  }

  // 根据给定对象属性 转换为html的element元素
  toElement(params = {}) {
    let defaultValue = {
      element: undefined,
      innerHtml: undefined,
      outerText: undefined,
    }

    let object = Object.keys(params).filter(key => { return !Object.keys(defaultValue).includes(key) })
    params = Object.assign(defaultValue, params)

    let element = document.createElement(params.element)
    for (let key of object) {
      let value = params[key]
      if (typeof value == 'object') value = JSON.stringify(value)
      element.setAttribute(key, value)
    }

    // 判断元素内容是否为空不能用!!,0等于假 0 == false
    if (![null, undefined, ''].includes(params.innerHtml)) {
      if (typeof params.innerHtml != 'object') {
        element.innerHTML = params.innerHtml
      } else if (!Array.isArray(params.innerHtml)) {
        let res = this.toElement(params.innerHtml)
        if (res[0]) element.innerHTML = res[1].outerHTML
      } else {
        params.innerHtml.forEach(item => {
          if (typeof item != 'object') {
            element.innerHTML = item
          } else if (!Array.isArray(item)) {
            element.appendChild(this.toElement(item)[1])
          }
        })
      }
    }
    if (!!params.outerText) element = { outerHTML: (element.outerHTML + params.outerText) }
    return [true, element]
  }

  uuid() {
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
      s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[8] = s[13] = s[18] = s[23] = "-";
    var uuid = s.join("");
    return uuid;
  }

  hashCode(string) {
    return string.split('').reduce((a, b) => { a = ((a << 5) - a) + b.charCodeAt(0); return a & a }, 0)
  }

  // 根据给出的当前节点 找到距离最近的预期类型的父节点
  // Graphic.getParentNode(this, 'TABLE')
  getParentNode(node, expectNodeName) {
    let current = node
    while (current.parentNode) {
      current = current.parentNode
      if (current.nodeName == expectNodeName) return current
    }
    return current
  }

  sudoku(params = {}) {
    let described = this.described.initialize({
      名称: '九宫格',
      描述: '根据提供的父对象,将其子元素进行等比例缩放,当点击元素时将全屏显示.',
      参数: {
        渲染对象: { 类型: 'string', 描述: '无效参数', 默认值: '无效参数' },
        每行元素个数: { 类型: 'integer', 默认值: 3 },
      }
    })
    if (!!params.showHow) return [true, described]

    let res = described.validate(params)
    if (!res[0]) return res

    if (described.父元素.childNodes.length == 0) return [false, '父对象内无子元素']

    let currentWidth = described.父元素.getBoundingClientRect().width
    let itemWidth = (currentWidth - 20) / described.每行元素个数
    let hwRatio = window.screen.height / window.screen.width
    let itemHeight = itemWidth * hwRatio

    let self = this
    described.父元素.childNodes.forEach(item => {
      // item.setAttribute('title', described.标题)
      item.style.width = itemWidth + 'px'
      item.style.height = itemHeight + 'px'
      item.style.float = 'left'

      switch (self.browser.browserName()) {
        case 'chrome':
          item.setAttribute('onclick', 'javascript:this.requestFullscreen();')
          break;
        case 'safair':
          item.setAttribute('onclick', 'javascript:this.webkitRequestFullscreen();')
          // TODO: safair退出全屏后存在元素未按比例缩小展示问题
          // 尚无法调试 F12导致浏览器卡死...
          // document.addEventListener("webkitfullscreenchange", function() {
          //   Graphic.sudoku({父元素: described.父元素})
          // });
          break;
      }

    })

    // TODO: 后期有需要改成参数配置
    described.父元素.style.height = Math.ceil(described.父元素.childNodes.length / described.每行元素个数) * itemHeight + 'px'
    described.父元素.style.marginTop = '20px'
    described.父元素.style.marginBottom = '20px'
    return [true, '']
  }

}