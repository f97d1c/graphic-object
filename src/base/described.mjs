// Base和Described相互引入 不能继承
import Base from '../base/base.mjs'
export default class Described {
  static initialize(params = {}) {
    return new Described(params)
  }

  constructor(params = {}) {
    // 对于需要用到的Base方法进行实现
    let methodNames = ['_typeof', 'multiType', 'uuid', 'toElement']
    methodNames.forEach(method => {
      this[method] = (params) => { return (new Base())[method](params) }
    })

    let defaultValue = {
      名称: undefined,
      描述: undefined,
      参数: {},
    }

    params = Object.assign(defaultValue, params)
    params.参数 = Object.assign({
      元素ID: { 类型: 'stringORinteger', 描述: "渲染后顶层元素id属性值,默认为uuid字符串", 默认值: this.uuid() },
      渲染对象: { 类型: 'keyValue', 默认值: undefined },
      父元素: { 类型: 'element', 描述: "渲染后顶层元素插入位置", 默认值: undefined },
      插入模式: { 类型: 'stringORfunction', 描述: '父元素调用append(后置),prepend(前置)或回调函数插入渲染后元素', 默认值: '后置' },
      标题: { 类型: 'string', 描述: "关于渲染对象相关说明", 默认值: '' },
      副标题: { 类型: 'string', 描述: "关于渲染对象相关说明", 默认值: '' },
    }, params.参数)

    for (let [key, value] of Object.entries(params)) {
      this[key] = value
    }

  }

  columns() {
    return Object.keys(this.参数)
  }

  validate(params = {}) {
    let res = this.validate_empty(params)
    if (!res[0]) return res

    res = this.validate_type(params)
    if (!res[0]) return res

    return [true, '']
  }

  validate_empty(params = {}) {
    if (!!!this.名称) return [false, "描述信息中[名称]参数不能为空"]
    if (!!!this.描述) return [false, "描述信息中[描述]参数不能为空"]

    for (let [key, value] of Object.entries(this.参数)) {
      if ((params[key] == undefined) && (value.默认值 != undefined)) {
        this[key] = value.默认值
      } else if ((Array.isArray(params[key])) && (params[key].length == 0) && value.默认值 != undefined) {
        this[key] = value.默认值
      } else if ((params[key] == undefined) && value.默认值 == undefined) {
        let template = key + '为必填项, 不能为空'
        if (!!value.描述) template += ', 用于: ' + value.描述
        template += '.'
        return [false, template]
      } else if ((Array.isArray(params[key])) && (params[key].length == 0) && value.默认值 == undefined) {
        let template = key + '为必填项, 不能为空'
        if (!!value.描述) template += ', 用于: ' + value.描述
        template += '.'
        return [false, template]
      } else {
        this[key] = params[key]
      }
    }
    return [true, '']
  }

  validate_type(params = {}) {
    for (let [key, value] of Object.entries(this.参数)) {
      let current_type = this._typeof(this[key])
      if (!(current_type == value.类型)) {
        if (this.multiType(value.类型)(current_type)) continue
        let template = key + ' 类型不符,要求类型为: ' + value.类型 + ', 传递参数类型为: ' + current_type + '.'
        return [false, template]
      }
    }
    return [true, '']
  }

  createEle(params) {
    let elementInfo = {}
    Object.keys(params).filter(key => { return !this.columns().includes(key) }).forEach(item => {
      elementInfo[item] = params[item]
    })

    let res = this.toElement(Object.assign({
      element: 'div',
      id: this.元素ID
    }, elementInfo))

    if (!res[0]) return res
    this.element = res[1]
    this.insertEle(this.element)

    return [true, '']
  }

  insertEle(ele) {
    let childEle = document.getElementById(ele.id || this.uuid())
    if (!!childEle) {
      childEle.remove()
    }

    if (this.插入模式 == '后置') {
      this.父元素.append(ele)
    } else if (this.插入模式 == '前置') {
      this.父元素.prepend(ele)
    } else {
      this.插入模式(ele)
    }
  }

}