import Base from './base/base.mjs'
import './base/base.css';

export default class Graphic extends Base {

  constructor(params={}) {
    super();
    let defaultValue = {
    }
    params = Object.assign(defaultValue, params)
    for (let [key, value] of Object.entries(defaultValue)) {
      this[key] = value
    }
  }

}
