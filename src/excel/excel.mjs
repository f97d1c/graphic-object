import Graphic from '../graphic.mjs'
import ExcelJS from 'exceljs'
import moment from 'moment'

export default class Excel extends Graphic {
  constructor(params = {}) {
    super()
  }

  // TODO: 方法加入示例页面
  // data = {招商中证白酒指数: [{"基金全称": "招商中证白酒指数证券投资基金(LOF)","基金简称": "招商中证白酒指数(LOF)","基金代码": "161725（主代码）","基金类型": "股票指数","发行日期": "2015年05月12日","成立日期/规模": "2015年05月27日 / 3.965亿份","资产规模": "499.14亿元（截止至：2021年03月31日）","份额规模": "394.9033亿份（截止至：2021年03月31日）","基金经理人": "侯昊","基金托管人": "中国银行","成立来分红": "每份累计0.00元（0次）","管理费率": "1.00%（每年）","托管费率": "0.22%（每年）","销售服务费率": "---（每年）","最高认购费率": "0.80%（前端）","业绩比较基准": "中证白酒指数收益率×95%+金融机构人民币活期存款基准利率(税后)×5%","跟踪标的": "中证白酒指数"}]}; Graphic.excel.toXlsx({渲染对象: data, 文件名: '招商中证白酒指数'})
  toXlsx(params = {}) {
    let described = this.described.initialize({
      名称: '生成xlsx文件',
      描述: '根据提供对象转换成xlsx文件.',
      参数: {
        父元素: { 类型: 'string', 描述: '无效参数', 默认值: '' },
        文件名: { 类型: 'string', 描述: '', 默认值: undefined },
        时间后缀: { 类型: 'string', 描述: '文件名后添加当前时间后缀, 参数值为时间格式, 不需要后缀传空字符串', 默认值: '_YYYYMMDDHHmm' },
        创建者: { 类型: 'string', 描述: '文件创建者', 默认值: 'GraphicObject' },
      }
    })
    if (!!params.showHow) return [true, described]

    let res = described.validate(params)
    if (!res[0]) return res

    let fileName = described.文件名
    if (!!described.时间后缀) {
      let now = new Date()
      let suffix = moment(now).format(described.时间后缀)
      fileName += suffix
    }

    let workbook = new ExcelJS.Workbook()

    workbook.creator = described.创建者
    workbook.lastModifiedBy = described.创建者
    workbook.created = new Date()
    workbook.modified = new Date()
    workbook.lastPrinted = new Date()
    // 将工作簿日期设置为 1904 年日期系统
    workbook.properties.date1904 = true
    // 在加载时强制工作簿计算属性
    workbook.calcProperties.fullCalcOnLoad = true
    // 工作簿视图控制在查看工作簿时 Excel 将打开多少个单独的窗口
    workbook.views = [
      { x: 0, y: 0, width: 10000, height: 20000, firstSheet: 0, activeTab: 0, visibility: 'visible' },
    ]

    for (let [key, value] of Object.entries(described.渲染对象)) {
      try {
        let sheet = workbook.addWorksheet(key.replace(/\*|\?|\:|\/|\[|\]/g,""))

        let columns = []
        Object.keys(value[0]).forEach(item => {
          // TODO: 后期优化 列宽根据最大元素长度决定
          columns.push({ header: item, key: item, width: 20 })
        })
        sheet.columns = columns

        value.forEach(item => { sheet.addRow(item) })
      } catch (error) {
        return [false, error.toString(), error]
      }

    }

    workbook.xlsx.writeBuffer({ base64: true })
      .then(function (xls64) {
        var a = document.createElement("a")
        var data = new Blob([xls64], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" })

        var url = URL.createObjectURL(data)
        a.href = url
        a.download = fileName + '.xlsx'
        document.body.appendChild(a)
        a.click()
        setTimeout(function () {
          document.body.removeChild(a)
          window.URL.revokeObjectURL(url)
        }, 0)

      })
      .catch(function (error) {
        return [false, error.message]
      })

    return [true, '']
  }
}